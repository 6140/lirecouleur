# -*- coding: utf-8 -*-

###################################################################################
# LireCouleur - outils d'aide à la lecture
#
# voir http://lirecouleur.arkaline.fr
#
# @author Marie-Pierre Brungard
# @version 5.0.0
# @since 2019
# https://forum.openoffice.org/en/forum/viewtopic.php?f=20&t=87057
#
# GNU General Public Licence (GPL) version 3
#
# LireCouleur is free software; you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation; either version 3 of the License, or (at your option) any later
# version.
# LireCouleur is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
# You should have received a copy of the GNU General Public License along with
# LireCouleur; if not, write to the Free Software Foundation, Inc., 59 Temple
# Place, Suite 330, Boston, MA  02111-1307  USA
###################################################################################

import unohelper

from com.sun.star.ui import XUIElementFactory

IMPL_NAME = "lire.libre.lirecouleur"
RESOURCE_NAME = "private:resource/toolpanel/lire.libre/lirecouleur"

class lirecouleurFactory(unohelper.Base, XUIElementFactory):
    """ Factory for LireCouleur """
    def __init__(self, ctx):
        self.ctx = ctx
    
    # XUIElementFactory
    def createUIElement(self, name, args):
        element = None
        if name == RESOURCE_NAME:
            frame = None
            parent = None
            for arg in args:
                if arg.Name == "Frame":
                    frame = arg.Value
                elif arg.Name == "ParentWindow":
                    parent = arg.Value
            if frame and parent:
                try:
                    import lirecouleur.lcpanel
                    element = lirecouleur.lcpanel.lirecouleurModel(self.ctx, frame, parent)
                except:
                    pass
        return element
    
    # XServiceInfo
    def getImplementationName(self):
        return IMPL_NAME
    def supportsService(self, name):
        return IMPL_NAME == name
    def supportedServiceNames(self):
        return (IMPL_NAME,)


g_ImplementationHelper = unohelper.ImplementationHelper()
g_ImplementationHelper.addImplementation(
    lirecouleurFactory, IMPL_NAME, (IMPL_NAME,),)
